
var UXAudio = function (my){
	my = my || {};
	var that = {};

	that.alloc = function () {
		return that;
	};
	that.init = function () {
		// AudioElement を作成
		var audio = new Audio();
		// メタデータ読み込み完了時に実行されるイベント
		audio.addEventListener("loadedmetadata",function (e){

			// 再生総時間を取得する
		//	console.log(audio.duration);

		});

		// サウンドファイルまでの URL アドレスを指定
		//audio.src = "/files/jihou.wav?1064821808";
		audio.src = 'jihou.wav';
		audio.load();


		var soundPlay = document.getElementById('sound-play');
		if ( soundPlay ) {
			soundPlay.addEventListener("click" , function(e){
				// 再生を開始する
				audio.play();
			});
		};
    	var soundPlay = document.getElementById('sound-play2');
		if ( soundPlay ) {
			soundPlay.addEventListener("click" , function(e){
				// 再生を開始する
				// audio.play();
				that.play2();
			});
		};

		var audio2 = new Audio();
		audio2.src = 'se.mp3';
		audio.load((e) => {
			console.log(e);
		});
		var soundPlay = document.getElementById('sound-play3');
		if ( soundPlay ) {
			soundPlay.addEventListener("click" , function(e){
				// 再生を開始する
				// audio.play();
				audio2.play();
			});
		};
		var playPromise = audio2.play();
		if (playPromise !== undefined) {
	      playPromise.then(_ => {
	        // Automatic playback started!
	        // Show playing UI.
			console.log("ok");
	      })
	      .catch(error => {
	        // Auto-play was prevented
	        // Show paused UI.
			console.log(error);
	      });
	    }
		// 再生を一時停止する
		//audio.pause();

		return that;
	};

  // 発話機能をインスタンス化
  var msg = new SpeechSynthesisUtterance();
  var voices = window.speechSynthesis.getVoices();
	that.play2Init = function () {
		// var ssu = new SpeechSynthesisUtterance();
		// ssu.text = 'クリスマスにはサンタさん来てくれるかな？';
		// ssu.lang = 'ja-JP';
		// speechSynthesis.speak(ssu);


// unsupported.
    if (!'SpeechSynthesisUtterance' in window) {
        alert('Speech synthesis(音声合成) APIには未対応です.');
        return;
    }


    // 以下オプション設定（日本語は効かないもよう。。）
    msg.voice = voices[7]; // 7:Google 日本人 ja-JP ※他は英語のみ（次項参照）
 /*
 0   Google US English en-US
 1   Google UK English Male en-GB
 2   Google UK English Female en-GB
 3   Google Español es-ES
 4   Google Français fr-FR
 5   Google Italiano it-IT
 6   Google Deutsch de-DE
 7   Google 日本人 ja-JP
 8   Google 한국의 ko-KR
 9   Google 中国的 zh-CN
 10  Alex en-US
 11  Agnes en-US
 12  Albert en-US
 13  Bad News en-US
 14  Bahh en-US
 15  Bells en-US
 16  Boing en-US
 17  Bruce en-US
 18  Bubbles en-US
 19  Cellos en-US
 20  Deranged en-US
 21  Fred en-US
 22  Good News en-US
 23  Hysterical en-US
 24  Junior en-US
 25  Kathy en-US
 26  Pipe Organ en-US
 27  Princess en-US
 28  Ralph en-US
 29  Trinoids en-US
 30  Vicki en-US
 31  Victoria en-US
 32  Whisper en-US
 33  Zarvox en-US
*/
    msg.volume = 1.0; // 音量 min 0 ~ max 1
    msg.rate = 1.0; // 速度 min 0 ~ max 10
    msg.pitch = 1.0; // 音程 min 0 ~ max 2

    msg.text = '問題：クリスマスにはサンタさん来てくれるかな？'; // 喋る内容
    msg.lang = 'ja-JP'; // en-US or ja-JP
    // msg.lang = 'en-US'; // en-US or ja-JP

    return that;
	};


  that.play2 = function () {
    // 発話実行
    speechSynthesis.speak(msg);

    // 終了時の処理
    msg.onend = function (event) {
        console.log('喋った時間：' + event.elapsedTime + 's');
    }
		return that;
	};


	return that;
};


(function() {
    document.addEventListener('DOMContentLoaded', function() {
		var uxAudio = new UXAudio().init().play2Init();
	});

}());


(function() {
	function f(wait) {
	    return new Promise((resolve) => {
	        setTimeout(() => {
	            console.log(wait);
	            resolve(wait * 2);
	        }, wait);
	    });
	};
	const arr = [
	    f, f, f,
	    (c) => Promise.all([f,f].map((x) => x(c)))
	];

	function log(x) {
	    console.log(x);
	    return x;
	}

	var promise = arr.reduce((m, p) => m.then(p), Promise.resolve(100))

	// 100 (100ms後)
	// 200 (さらに200ms後)
	// 400 (さらに400ms後)
	// 800 (さらに800ms後)

	const summate = (x) => x.reduce((a,b) => a+b);
	promise = promise.then(summate).then(log);
	// 3200 (immediately)
}());


(function() {
return;
 	function myPromise2(num) {
		return new Promise((resolve, reject) => {
			setTimeout(function() {
				resolve(num)
			}, num*100);
		});
    }

	async function fn() {
	  const urls = [30,10];
	  await Promise.all( urls.map(myPromise2) ).then((values)=>{
		  console.log(values);
	  });
	}
	fn();
}());


(function() {
return;
	function *generator() {
		yield 1;
		yield 2;
		return 3;
	}
	const g = generator();
	console.log(g);
	console.log(g.next());
	console.log(g.next());
	console.log(g.next());


}());
